package com.classpath.inventorymicroservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/inventory")
@Slf4j
public class InventoryController {

    private int counter = 1000;

    @PostMapping
    public int updateInventory(){
        log.info("Updating the inventory :: ");
        return --counter;
    }
}
