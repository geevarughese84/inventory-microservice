package com.classpath.inventorymicroservice.event;

import com.classpath.inventorymicroservice.model.EventType;
import com.classpath.inventorymicroservice.model.Order;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Data
@Getter
@RequiredArgsConstructor
public class OrderEvent {
    private final Order order;
    private final EventType eventType;
    private final LocalDateTime timestamp;
}